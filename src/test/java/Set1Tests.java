package org.bitbucket.zune;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.rules.Timeout;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Simple unit tests for Zune
 */
public class Set1Tests {

    /** Limit each test to 10 millis */
    @Rule
    public Timeout globalTimeout = new Timeout(10);

    /**
     *  Logger for this test suite
     */
    Logger logger = LoggerFactory.getLogger(Set1Tests.class);

    private Zune z = new Zune("franck");
    private Zune z1 = new Zune("");
    private Zune z2 = new Zune();

    /**
     * Tests for computeYear
     */
    @Test
    public void computeTest1() { assertEquals(1980, z.computeYear(330) ); }

    @Test
    public void computeTest2() { assertEquals(1981, z.computeYear(400) ); }

    @Test
    public void computeTest3() { assertEquals(1984, z.computeYear(1500) ); }

    @Test
    public void computeTest4() { assertEquals(1985, z.computeYear(1828) ); }

    @Test
    public void computeTest5() { assertEquals(1980, z.computeYear(0) ); }

    @Test
    public void computeTest6() { assertEquals(1980, z.computeYear(-1) ); }

    @Test
    //Used to cause infinite loop error
    public void computeTest7() { assertEquals(1981, z.computeYear(366) ); }

    @Test
    //Still causes infinite loop (for max test coverage)
    public void computeTest8() { assertEquals(1980, z.computeYear(365) ); }

    /**
    Tests for mutants
     */
    @Test
    public void computeMutantTest1() { assertEquals(1980, z.computeYear1(330) ); }

    @Test
    public void computeMutantTest2() { assertEquals(1981, z.computeYear1(400) ); }

    @Test
    public void computeMutantTest3() { assertEquals(1984, z.computeYear2(1500) ); }

    @Test
    public void computeMutantTest4() { assertEquals(1985, z.computeYear2(1828) ); }

    @Test
    public void computeMutantTest5() { assertEquals(1980, z.computeYear3(0) ); }

    @Test
    public void computeMutantTest6() { assertEquals(1980, z.computeYear4(-1) ); }

    @Test
    //Used to cause infinite loop error
    public void computeMutantTest7() { assertEquals(1981, z.computeYear5(366) ); }

    @Test
    //Still causes infinite loop (for max test coverage)
    public void computeMutantTest8() { assertEquals(1980, z.computeYear5(365) ); }


    /**
     Tests for isLeapYear
     */

    @Test
    public void leapTest1() { assertEquals(true, z.isLeapYear(2164) ); }

    @Test
    public void leapTest2() { assertEquals(true, z.isLeapYear(2400) ); }

    @Test
    public void leapTest3() { assertEquals(false, z.isLeapYear(2100) ); }

    @Test
    public void leapTest4() { assertEquals(false, z.isLeapYear(2001) ); }

    @Test
    public void leapTest5() { assertEquals(false, z.isLeapYear(-1) ); }

    @Test
    public void leapTest6() { assertEquals(true, z.isLeapYear(0) ); }

}
